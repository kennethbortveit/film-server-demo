module.exports = function(app, mongoose, jwt){
	
	var serieSchema = mongoose.Schema({
		title: String,
		episode: String,
		videoUrl: String,
		imageUrl: String
	}, {collection: 'series'});

	var Serie = mongoose.model('Serie', serieSchema);

	app.get('/series', function(request, response){
		if(request.query.token == 'null'){
			response.send({msg: 'User not logged in...'});
		}
		else if(!request.query.token){
			response.send({msg: 'Invalid token.'});
		}
		else if(jwt.verify(request.query.token, 'secret')){
			Serie.find({}, function(error, series){
				if(error){
					response.send({msg: 'Something wrong with the database...'});
				} else {
					response.send(series);	
				}
			});
		} else {
			response.send({msg: 'Invalid token...'});
		}
	});
}
