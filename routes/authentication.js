module.exports = function(app, mongoose, bcrypt, jwt){
	
	var userSchema = mongoose.Schema({
		username: String,
		password: String
	}, {collection: 'users'});

	var User = mongoose.model('User', userSchema);

	app.post('/login', function(request, response){
		
		User.findOne({username: request.body.username}, function(error, user){
			if(error){
				response.send({msg: 'Something wrong with the database.'});
			} else if(user == null){
				response.send({msg: 'Wrong username or password'});
			} else if(!bcrypt.compareSync(request.body.password, user.password)){
				response.send({msg: 'Wrong username or password'});
			} else {
				var token = jwt.sign(user, 'secret', {expiresIn: 7200});
				response.send({msg: 'Login successfull.', token: token});	
			}
		});
	});

	app.post('/new-user', function(request, response){
		User.collection.insert({
			username: request.body.username,
			password: bcrypt.hashSync(request.body.password, 10),
			firstname: request.body.firstname,
			lastname: request.body.lastname,
			email: request.body.email
		}, function(error, user){
			if(error){
				response.send(error);
			} else {
				response.send(user);
			}
		});
	});
}
