module.exports = function(app, mongoose, jwt){
	var movieSchema = mongoose.Schema({
		title: String,
		videoUrl: String,
		pictureUrl: String
	}, {collection: 'movies'});
	var Movie = mongoose.model('Movie', movieSchema);

	app.get('/movies', function(request, response){
		if(request.query.token == 'null'){
			response.send({msg: 'User not logged in...'});
		}
		else if(!request.query.token){
			response.send({msg: 'Invalid token.'});
		}
		else if(jwt.verify(request.query.token, 'secret')){
			Movie.find({}, function(error, movies){
				if(error){
					response.send({msg: 'Error with the database.'});
				} else {
					response.send(movies);
				}
			});
		} else {
			response.send({msg: 'Token is no good...'});
		}
	});
}
